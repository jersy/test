<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Laravel PHP Framework</title>
</head>
<body>

	@if (Session::get('success'))
		{{ Session::get('success') }}
	@endif

	@if ($errors->all())
		@foreach ($errors->all() as $error)
			<p>{{ $error }}</p>
		@endforeach
	@endif

	<div class="welcome">
		<h2>Оформление подписки</h2>
		<span class="form-note">* - поля обязательные для заполнения</span>
		{{ Form::open(array('route'=>'validation')) }}
			<p>
				{{ Form::radio('subscription_time', '3', true) }}3 мес. - <b>65 грн.</b>
				{{ Form::radio('subscription_time', '6') }}6 мес. - <b>130 грн.</b>
			</p>
			<p>
				{{ Form::text('username', null,  array('placeholder'=>'Фамилия, имя, отчество*', 'class'=>'form-control')) }}
			</p>
			<p>
				{{ Form::email('email', null,  array('placeholder'=>'Адрес электронной почты*', 'class'=>'form-control')) }}
			</p>
			<p>
				{{ Form::text('phone', null,  array('placeholder'=>'Контактный номер телефона*', 'class'=>'form-control')) }}
			</p>
			<p>
				{{ Form::checkbox('addr'); }}
				Я не хочу получать напечатанный выпуск газеты
			</p>
			<p>
				Адрес доставки газеты:
			</p>
			<p>
				{{ Form::text('street', null,  array('placeholder'=>'Улица (проспект, переулок)*', 'class'=>'form-control')) }}
			</p>
			<p>
				{{ Form::text('house', null,  array('placeholder'=>'Дом (корпус)*', 'class'=>'form-control')) }}
			</p>
			<p>
				{{ Form::text('room', null,  array('placeholder'=>'Квартира', 'class'=>'form-control')) }}
			</p>
			<p>
				{{ Form::submit('Далее') }}
			</p>

		{{ Form::close() }}

	</div>
</body>
</html>
