<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
<header>
    <div class="wrapper-header">
        <div class="logo">
            <a href="#"><img src="img/logo5.png" alt="logo"></a>
        </div>
        <div class="search">
            <div class="left-img">
                <img src="img/humans.png" alt="humans">
            </div>
            <div class="search-block">
                <input type="text" name="search" placeholder="Поиск">
            </div>
            <div class="right-img">
                <img src="img/elipce.jpg" alt="elipce">
                <div>Регион</div>
                <img class="search-img" src="img/search-img.png" alt="search-img">
            </div>
        </div>
        <div class="header-right">
            <a href="#">
                <img class="avatar" src="img/profile.png" alt="profile">
                <div class="profile">Профиль</div>
                <img class="arrow" src="img/arrow.png" alt="arrow">
            </a>
        </div>
    </div>
</header>

<div class="middle">
    <div class="wrapper-middle">
        <span>Портал объявлений</span>
        <a href="#">
            <div class="green-btn-ad">
                <img src="img/ad-arrow.png" alt="ad-arrow">
                <span>Разместить обявление</span>
            </div>
        </a>
    </div>
</div>

<div class="content">
    <div class="wrapper-content">
        <div class="element">
            <a href="#">
                <div class="border">
                    <img src="img/auto.png" alt="auto">
                </div>
                <span class="category-name">Транспорт</span>
                <span class="category-count">(4234)</span>
            </a>
        </div>
        <div class="element">
            <a href="#">
                <div class="border">
                    <img src="img/auto.png" alt="auto">
                </div>
                <span class="category-name">Транспорт</span>
                <span class="category-count">(4234)</span>
            </a>
        </div>
        <div class="element">
            <a href="#">
                <div class="border">
                    <img src="img/auto.png" alt="auto">
                </div>
                <span class="category-name">Транспорт</span>
                <span class="category-count">(4234)</span>
            </a>
        </div>
        <div class="element">
            <a href="#">
                <div class="border">
                    <img src="img/auto.png" alt="auto">
                </div>
                <span class="category-name">Транспорт</span>
                <span class="category-count">(4234)</span>
            </a>
        </div>
        <div class="element">
            <a href="#">
                <div class="border">
                    <img src="img/auto.png" alt="auto">
                </div>
                <span class="category-name">Транспорт</span>
                <span class="category-count">(4234)</span>
            </a>
        </div>
        <div class="element">
            <a href="#">
                <div class="border">
                    <img src="img/auto.png" alt="auto">
                </div>
                <span class="category-name">Транспорт</span>
                <span class="category-count">(4234)</span>
            </a>
        </div>
        <div class="element">
            <a href="#">
                <div class="border">
                    <img src="img/auto.png" alt="auto">
                </div>
                <span class="category-name">Транспорт</span>
                <span class="category-count">(4234)</span>
            </a>
        </div>
        <div class="element">
            <a href="#">
                <div class="border">
                    <img src="img/auto.png" alt="auto">
                </div>
                <span class="category-name">Транспорт</span>
                <span class="category-count">(4234)</span>
            </a>
        </div>
        <div class="element">
            <a href="#">
                <div class="border">
                    <img src="img/auto.png" alt="auto">
                </div>
                <span class="category-name">Транспорт</span>
                <span class="category-count">(4234)</span>
            </a>
        </div>
        <div class="element">
            <a href="#">
                <div class="border">
                    <img src="img/auto.png" alt="auto">
                </div>
                <span class="category-name">Транспорт</span>
                <span class="category-count">(4234)</span>
            </a>
        </div>
        <div class="element">
            <a href="#">
                <div class="border">
                    <img src="img/auto.png" alt="auto">
                </div>
                <span class="category-name">Транспорт</span>
                <span class="category-count">(4234)</span>
            </a>
        </div>
        <div class="element">
            <a href="#">
                <div class="border">
                    <img src="img/auto.png" alt="auto">
                </div>
                <span class="category-name">Транспорт</span>
                <span class="category-count">(4234)</span>
            </a>
        </div>
        <div class="element">
            <a href="#">
                <div class="border">
                    <img src="img/auto.png" alt="auto">
                </div>
                <span class="category-name">Транспорт</span>
                <span class="category-count">(4234)</span>
            </a>
        </div>
        <div class="element">
            <a href="#">
                <div class="border">
                    <img src="img/auto.png" alt="auto">
                </div>
                <span class="category-name">Транспорт</span>
                <span class="category-count">(4234)</span>
            </a>
        </div>
        <div class="element">
            <a href="#">
                <div class="border">
                    <img src="img/auto.png" alt="auto">
                </div>
                <span class="category-name">Транспорт</span>
                <span class="category-count">(4234)</span>
            </a>
        </div>
    </div>
</div>

<div class="pre-footer">
    <div class="wrapper-pre-footer">
        <div class="left-pre-footer">
            <span>Хотите продать?</span>
        </div>
        <div class="middle-pre-footer">
            <p>В интернете есть много площадок для размещения объявлений.
                Но до сих пор не было такой удобной. Инновации, удобство и простота - это то,
                чем отличается портал Obyava.ua.
            </p>
        </div>
        <div class="right-pre-footer">
            <a href="#">
                <div class="footer-btn-ad">
                    <img src="img/ad-arrow.png" alt="ad-arrow">
                    <span>Разместить обявление</span>
                </div>
            </a>
        </div>
    </div>
</div>

<footer>
    <div class="wrapper-footer">
        <div class="logo-footer">
            <img src="img/logo5.png" alt="logo">
        </div>
        <ul>
            <li><a href="#">About us</a></li>
            <li><a href="#">Mobile website</a></li>
            <li><a href="#">FAQ</a></li>
            <li><a href="#">Paid servicies</a></li>
            <li><a href="#">Terms and conditions</a></li>
            <li><a href="#">Help</a></li>
        </ul>
    </div>
</footer>

</body>
</html>