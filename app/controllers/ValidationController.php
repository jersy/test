<?php

class ValidationController extends Controller
{
	public function validation()
	{
		$messages = array(
			'username.required' => 'Имя обязательно для заполнения!',
			'username.min' => 'Минимальная длина имени :min символа!',
			'username.max' => 'Максимальная длина имени :max символов!',
			'email.required' => 'Почта обязательна для заполнения!',
			'email.max' => 'Максимальная длина почтового ящика :max символов!',
			'email.email' => 'Не корректный формат почтового ящика!',
			'phone.required' => 'Телефон обязателен для заполнения!',
			'phone.regex' => 'Не корректный телефон!',
			'phone.min' => 'Минимальная длина телефона :min символов!',
			'phone.max' => 'Максимальная длина телефона :max символов!',
			'street.required' => 'Адрес обязателен для заполнения!',
			'street.min' => 'Минимальная длина адреса :min символов!',
			'street.max' => 'Максимальная длина адреса :max символов!',
			'house.required' => 'Номер дома обязателен для заполнения!',
			'house.numeric' => 'Не корректный номер дома!',
			'house.min' => 'Минимальная длина адреса :min символ!',
			'house.max' => 'Максимальная длина телефона :max символов!',
		);

		$rules['username'] = 'required|min:3|max:50';
		$rules['email'] = 'required|email|max:50';
		$rules['phone'] = 'required|regex:/\+?\d+/|min:10|max:12';

		if (!Input::get('addr')) {
			$rules['street'] = 'required|min:3|max:50';
			$rules['house'] = 'required|numeric|min:1|max:5';
		}

		$validation = Validator::make(Input::All(), $rules, $messages);

		if ($validation->fails())
		{
			return Redirect::to('/')->withErrors($validation)->withInput();
		}

		Session::flash('success', 'Подписка успешно оформленна!');
		return Redirect::route('home');
	}
}
